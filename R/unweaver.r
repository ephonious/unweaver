#' unweaver.
#'
#' @name unweaver
#' @docType package
NULL

#' Enrichment (Z-score) of genes across 73 cell types and tissues.
#' 
#' Tissue specific enrichmet scores for ~14,000 Affymetrix U133A probesets across 73
#' human tissues (hematopoetic and CNS only). See:
#' Benita Y, Cao Z, Giallourakis C, Li C, Gardet A, et al. (2010) Gene enrichment 
#' profiles reveal T-cell development, differentiation, and lineage-specific 
#' transcription factors including ZBTB25 as a novel NF-AT repressor. 
#' Blood 115: 5376-5384. doi:10.1182/blood-2010-01-263855.
#' 
#' A dataset containing the tissue specific enrichment Z-scores of ~14,000 probesets
#' across 73 hematopoetic and CNS cell-types in long format. The variables are as 
#' follows:
#' 
#' \itemize{
#'   \item celltype name of cell-type (n = 73).
#'   \item category. cell-types with similar tissue origins are grouped into categories (n = 17).
#'   \item entrezgene. EntrezGene ID mapping for all U133A probeset identifiers (n = 10419).
#'   \item probeset. Affymetrix U133A identifiers (n = 14183).
#'   \item score. normalized z-score representing relative enrichment (see Benita et al. for methodology).
#' }
#' 
#' @docType data
#' @keywords datasets
#' @name tissues
#' @usage data(tissues)
#' @format A data frame with 1245672 rows and 5 variables (long format)
NULL