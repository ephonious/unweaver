# Collection of functions to assess tissue specificity of deconvolved cell 
# type-specific expression
# @author C.Shannon

#' Retrieve significant probesets given a cell type contrast, direction and cutoff
#' 
#' Given a cell type contrast name, direction and cutoff, retrieve a list of 
#' differentially expressed probesets in that cell type at that significance cutoff.
#' Optionally, return a named vector of their fold changes.
#' 
#' @param fit returned by fdeconv.
#' @param fit.fdr returned by fdeconv.fdr or fdeconv.fdr.mc.
#' @param cell the cell type contrast of interest.
#' @param direction one of "greater" or "less".
#' @param cutoff the significance cutoff to use (default = 0.3).
#' @param logfc whether cell type-specific probeset fold-changes should be returned 
#'   or not
#' @return a named and ordered vector of probeset identifiers.
#' @export
get.probesets <- function(fit, fit.fdr, cell, direction = c("greater", "less"), cutoff = 0.3, logfc = FALSE){
  direction <- match.arg(direction)
  cell.index <- match(tolower(cell), tolower(names(fit$params$ccbar)))
  switch(direction,
         greater = {
           val <- head(which(fit.fdr$fdr$cssam$greater[cell.index, ] <= cutoff), 1)
           val <- fit.fdr$cutp$cssam[cell.index, val]
           val <- which(fit$rhat[cell.index, ] >= val)
           coefs <- fit$rhat[cell.index, val]
           names(coefs) <- colnames(fit$Ghat)[val]
           coefs <- sort(coefs, decreasing = T)
           if (logfc) 
             return(coefs) 
           else 
             return(names(coefs))
         },
         less = {
           val <- head(which(fit.fdr$fdr$cssam$less[cell.index, ] <= cutoff), 1)
           val <- fit.fdr$cutp$cssam[cell.index, val]
           val <- which(fit$rhat[cell.index, ] <= -val)
           coefs <- fit$rhat[cell.index, val]
           names(coefs) <- colnames(fit$Ghat)[val]
           coefs <- sort(coefs)
           if (logfc)
             return(coefs)
           else
             return(names(coefs))
         })
}

#' Compute enrichment of a probeset list across various tissue types.
#' 
#' Given a list of Entrez gene ids, compute tissue enrichment using the Benita et al.
#' dataset. This function exposes two alternative implementations: one computes 
#' median enrichment score across the provided gene identifiers, the other computes
#' the p-value of the hypergeometric test of the overlap of the provided gene
#' identifiers and the top n quantile (specified by the "cutoff" value) most enriched
#' probesets in each tissue.
#' 
#' @param genes a vector of Entrez gene identifiers.
#' @param type "median" or "fisher", which enrichment procedure to return.
#' @param cutoff only used when type = "fisher". The size of the tissue specific gene
#'   set to use when computing significance of the overlap. Genes are ranked from 
#'   most to least tissue-specific and the top "cutoff" are used to generate tissue
#'   specific gene sets.
#' @return a data frame with the following variables "list", the names of the lists
#'   of genes submitted for enrichment analysis (or "genes" if a character vector of 
#'   gene identifiers was submitted), "celltype", "category", the thematic tissue 
#'   group to which the celltype belongs, and "enrichment", either the median 
#'   enrichment score of the submitted vector of probesets in each celltype, or the 
#'   (unadjusted) p-value of the hypergeometric test of the overlap of the provided 
#'   probesets and the top n percent most enriched probesets in each tissue.
#' @export
compute.enrichment <- function(genes, type = c("median", "fisher"), cutoff = 100) {
  type <- match.arg(type)
  if(!is.list(genes))
    genes <- list("genes" = genes)
  switch(type, 
         median = ldply(genes, .id = "list", function(x) compute.median.enrichment(x)),
         fisher = ldply(genes, .id = "list", function(x) compute.fisher.enrichment(x, cutoff)))    
}

#' Compute enrichment of a probeset list across various tissue types.
#' 
#' Given a list of Entrez gene ids, compute median tissue enrichment using the Benita
#' et al. dataset.
#' 
#' @param genes a vector of Entrez gene identifiers.
#' @return a data frame with the following variables "celltype", "category" (the
#'   thematic tissue group to which the celltype belongs) and "enrichment", the
#'   median enrichment score of the submitted vector of probesets in each celltype.
#' @import plyr
compute.median.enrichment <- function(genes) {
  data(tissues)
  ddply(tissues, .(celltype), function(x) {
    data.frame(category = unique(x$category), enrichment=median(x$score[x$entrezgene %in% genes], na.rm = T))
  })
}

#' Compute enrichment of a probeset list across various tissue types.
#' 
#' Fisher's exact test of the null independence of the tissue reference list (top nth
#' [cutoff] quantile of genes from each tissue by enrichment using the Benita et al. 
#' dataset) and given list of Entrez gene ids.
#' 
#' @param genes a vector of Entrez gene identifiers.
#' @param cutoff number of top enriched probesets to compose the tissue reference
#'   gene lists.
#' @return a data frame with the following variables: "celltype", "category", the
#'   thematic tissue group to which the celltype belongs, and "enrichment", the 
#'   (unadjusted) p-value of the hypergeometric test of the overlap of the provided
#'   probesets and the top n percent most enriched probesets in each tissue.
#' @import plyr
compute.fisher.enrichment <- function(genes, cutoff) {
  data(tissues)
  tissues <- ddply(tissues, .(celltype), function(x) head(x[order(x$score, decreasing = T), ], n = cutoff))  
  ddply(tissues, .(celltype), function(x) {  
    # Fisher's exact
    table <- data.frame(row.names = levels(tissues$entrezgene),
                        reference = levels(tissues$entrezgene) %in% as.character(x$entrezgene),
                        genes = levels(tissues$entrezgene) %in% genes)
    p <- fisher.test(table$reference, table$genes, alternative = "greater")$p.value
    # fix for precision issue
    p[p >= 1] <- 1
    data.frame(category = unique(x$category), enrichment = p)
  })
}

#' Plot the output of either compute.enrichment as a heatmap.
#' 
#' @param enrichment a data.frame of enrichment values across all tissues in the 
#'   "tissues" data. The object returned by compute.enrichment().
#' @param type Specifies the type of plot to return, barplot or heatmap.
#' @return a ggplot object.
#' @import ggplot2
#' @export
enrichment.plot <- function(enrichment, type = c("barplot", "heatmap")) {  
  # check if p-values or median enrichment scores
  if(min(enrichment$enrichment) >=  0 & max(enrichment$enrichment) <= 1) {
    enrichment$enrichment <- -log10(enrichment$enrichment + .Machine$double.xmin)
    guide <- "-log10(p)"
    hline <- -log10(0.05)
  } else {
    guide <- "enrichment"
    hline <- 0
  }
  
  # type of plot
  switch(match.arg(type),
         barplot = (ggplot(enrichment, aes(celltype, enrichment)) +
                      geom_bar(aes(fill = category), stat = "identity", position = "dodge", show_guide=F) +
                      geom_hline(y = hline, linetype = "dashed") +
                      scale_y_continuous(guide) +
                      facet_grid(list~category, scale="free_x", space = "free_x") +
                      theme(axis.text.x = element_text(angle = 45, size = 6, hjust = 1, vjust = 1),
                            strip.text.x = element_text(angle = 90, size = 10))),
         heatmap = (ggplot(enrichment, aes(" ", celltype)) +
                      geom_raster(aes(fill = enrichment)) +
                      scale_x_discrete(expand = c(0,0)) +
                      scale_y_discrete(expand = c(0,0)) +
                      scale_fill_gradient2(low = "blue", mid = "white", high = "red", name = guide) +
                      facet_grid(category~list, scale = "free", space = "free_x") +
                      theme(axis.title.x = element_blank(),
                            axis.text.x = element_blank(),
                            axis.ticks.x = element_blank(),
                            strip.text.y = element_text(angle = 0, size = 10))))
}

#' Plot the output of either compute.enrichment as a barplot.
#' 
#' @param enrichment a data.frame of enrichment values across all tissues in the 
#'   "tissues" data. The object returned by compute.enrichment().
#' @param type Specifies the type of plot to return, barplot or heatmap.
#' @return a ggplot object.
#' @import ggplot2
#' @import dplyr
#' @export
enrichment.plot.2 <- function(enrichment, type = c("barplot", "heatmap")) {
  enrichment %>% 
    filter(!category %in% c("rbcs", "blood", "tonsils", "lymph", "spleen", "thymus", "cns")) %>%
    mutate(category = factor(category)) %>%
    ggplot(aes(celltype, -log10(enrichment))) +
    geom_bar(stat = "identity", aes(fill = category), show_guide = F) +
    geom_hline(y = -log10(0.01), linetype = "dashed", colour = "black") +
    facet_grid(list~category, scale = "free_x", space = "free_x") +
    scale_fill_brewer(palette = "Spectral") +
    theme_few() +
    theme(axis.text.x = element_text(angle = 45, vjust = 1, hjust = 1, size = 8),
          strip.text.x = element_text(angle = 0, vjust = 0.5, hjust = 0))
}